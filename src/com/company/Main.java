package com.company;

class Main {

    public static void main(String[] args) {
        BattleField battleField = new BattleField();
        Controller controller = new Controller(battleField);
        controller.addShips();
        battleField.showResultField();
        System.out.println(controller.resultInfoShips);
    }
}
