package com.company;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Alina on 13.10.2017.
 */
class Ship {

    private String name;
    private int shots;
    int length;
    ArrayList<Cell> cells;

    Ship(String name, int length) {
        this.name = name;
        this.length = length;
        initCells(length);
    }

    private void initCells(int length) {
        cells = new ArrayList<>();
        while (cells.size() < length) cells.add(new Cell());
    }

    void shootShip() {
        Random rand = new Random();
        shots = rand.nextInt(length + 1);
        if (shots != 0) {
            int tmp = shots;
            for (int i = 0; i < tmp; i++) {
                int shotCell = rand.nextInt(length);
                if (!(cells.get(shotCell).state.equals("S"))) {
                    cells.get(shotCell).state = "S";
                } else {
                    tmp++;
                }
            }
        }
    }

    String shipInfo() {
        String state;
        if (shots == 0) {
            state = "Unshot";
        } else {
            state = "Shot";
        }
        int health = length - shots;
        return "Name: " + name + '\t' + "Size: " + length + '\t' + "State:" + '\t' + state + '\t' + "Health:  "
                + health + "/" + length;
    }
}
