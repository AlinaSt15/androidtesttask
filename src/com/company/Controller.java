package com.company;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Alina on 19.09.2017.
 */
class Controller {

    private BattleField battleField;
    private List<Ship> ships;
    private boolean isAdded;
    String resultInfoShips;

    Controller(BattleField battlefield) {
        this.battleField = battlefield;
        resultInfoShips = "\n";
    }

    private void initShips() {
        ships = new ArrayList<>();
        ships.add(new Ship("Four-decker", 4));
        ships.add(new Ship("Three-decker", 3));
        ships.add(new Ship("Three-decker", 3));
        ships.add(new Ship("Double-decker", 2));
        ships.add(new Ship("Double-decker", 2));
        ships.add(new Ship("Double-decker", 2));
        ships.add(new Ship("Single-decker", 1));
        ships.add(new Ship("Single-decker", 1));
        ships.add(new Ship("Single-decker", 1));
        ships.add(new Ship("Single-decker", 1));
    }

    void addShips() {
        initShips();
        for (Ship ship : ships) {
            ship.shootShip();
            isAdded = false;
            Point point = new Point();
            int orientation = (int) (Math.random());
            if ((orientation % 2) == 0) {
                while (!isAdded) {
                    putShipHorizontally(ship, point);
                }
            } else {
                while (!isAdded) {
                    putShipVertically(ship, point);
                }
            }
            resultInfoShips += ship.shipInfo() + '\n';
        }
    }

    private void putShipVertically(Ship ship, Point point) {
        setPoint(point);
        if ((point.x + ship.length <= battleField.field.length - 2) && (isPlacedVertically(ship.length, point))) {
            for (int i = 0; i < ship.length; i++) {
                battleField.field[point.x + i][point.y] = ship.cells.get(i).state;
            }
            isAdded = true;
        }
    }

    private void putShipHorizontally(Ship ship, Point point) {
        setPoint(point);
        if ((point.y + ship.length <= battleField.field.length - 2) && (isPlacedHorizontally(ship.length, point))) {
            for (int i = 0; i < ship.length; i++) {
                battleField.field[point.x][point.y + i] = ship.cells.get(i).state;
            }
            isAdded = true;
        }
    }

    private void setPoint(Point point) {
        point.x = ThreadLocalRandom.current().nextInt(1, battleField.field.length - 1);
        point.y = ThreadLocalRandom.current().nextInt(1, battleField.field.length - 1);
    }

    private boolean isPlacedVertically(int shipLength, Point point) {
        boolean result = true;
        for (int i = -1; i < shipLength + 1; i++) {
            if (isNotCellZero(point.x + i, point.y) || isNotCellZero(point.x + i, point.y + 1)
                    || isNotCellZero(point.x + i, point.y - 1)) {
                result = false;
                break;
            }
        }
        return result;
    }

    private boolean isPlacedHorizontally(int shipLength, Point point) {
        boolean result = true;
        for (int i = -1; i < shipLength + 1; i++) {
            if (isNotCellZero(point.x, point.y + i) || isNotCellZero(point.x - 1, point.y + i)
                    || isNotCellZero(point.x + 1, point.y + i)) {
                result = false;
                break;
            }
        }
        return result;
    }

    private boolean isNotCellZero(int x, int y) {
        return !(battleField.field[x][y].equals("0"));
    }
}
