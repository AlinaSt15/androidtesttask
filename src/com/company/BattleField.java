package com.company;

/**
 * Created by Alina on 19.09.2017.
 */
class BattleField {

    private final int ROW_COUNT = 12;
    private final int COLUMN_COUNT = 12;

    final String[][] field = new String[ROW_COUNT][COLUMN_COUNT];

    BattleField() {
        initField();
    }

    private void initField() {
        for (int i = 1; i < field.length - 1; i++) {
            for (int j = 1; j < field[i].length - 1; j++) {
                field[i][j] = "0";
            }
        }
        for (int i = 0; i < field.length; i++) {
            field[i][0] = "-";
            field[0][i] = "-";
            field[field.length - 1][i] = "-";
            field[i][field[i].length - 1] = "-";
        }
    }

    void showResultField() {
        String[] letters = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J"};
        System.arraycopy(letters, 0, field[0], 1, letters.length);
        for (int i = 0; i < field.length - 1; i++) {
            field[i][0] = String.valueOf(i);
            field[field.length - 1][i] = "";
            field[i][field[i].length - 1] = "";
        }
        field[0][0] = " ";
        for (int i = 0; i < field.length - 1; i++) {
            for (int j = 0; j < field[i].length - 1; j++) {
                if (i == 10 && j == 0) {
                    System.out.print(field[i][j] + " ");
                } else {
                    System.out.print(field[i][j] + "  ");
                }
            }
            System.out.println();
        }
    }
}
